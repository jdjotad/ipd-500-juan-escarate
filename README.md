# Implementación en FPGA de ADMM
<div align="justify">
Este repositorio forma parte de la tesis titulada <b>IMPLEMENTACIÓN EN FPGA DE CONTROL PREDICTIVO BASADO EN MODELOS PARA UN CONVERTIDOR DC/AC CONECTADO A UN FILTRO LC EN DERS</b> para optar al grado de Magister en Ciencias de la Ingeniería Electrónica mención Computadores.
</div>
<br></br>
<div align="justify">
Este proyecto está a cargo del grupo de investigación compuesto por los profesores César Silva, Gonzalo Carvajal y Juan Agüero.
</div>
