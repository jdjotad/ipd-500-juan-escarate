clear all
close all
clc

global x_ADMM z_ADMM u_ADMM QP_vars counter solver ADMM_iters curr_rest
max_steps = 2501;
QP_vars = {};
QP_vars.Q = zeros(16,16);
QP_vars.q = zeros(16,1);
QP_vars.A = zeros(74,16);
QP_vars.c = zeros(max_steps,74,1);
QP_vars.x_QP  = zeros(max_steps,16,1);
QP_vars.rho   = zeros(max_steps,1);

%% VALORES BASE:
% Sb=5000 %VA
% Vb=260 %base pico por hp
% Ib=12.8 %A
% Zb=20.3 %ohm  Vb/Ib
% Wb=1 %rad/s  50 Hz nominal
% Vdc=630 %V base dc

%% VALORES NOMINALES (per unit):
% Vs=1 % Vs/Vb
% Rf=0.0032   % Rf/Zb 
% Lf=0.045    % LfWb/Zb
% Cf=0.095    % CfWbZb
% Vdc=1       % Vdc/Vb
% T=200*10^(-6)      % Sampling time

%%
% Valores de Planta laboratorio filtro LC
Rf  = 0.065;    % Resistencia Filtro 
Lf  = 3e-3;     % Inductancia Filtro 
Cf  = 15e-6;    % Capacitancia Filtro 
Vdc = 100;      % Voltaje DC de entrada filtro / Salida 1.15*VDC/2
T   = 200e-6;   % Periodo de muestreo
f   = 50;       % Frecuencia Hz
Wb  = 2*pi*f;   % Frecuencia rad/s   
RL  = 23.6;     % Resistencia de carga


% Modelo espacio estado filtro LC (Continuo)
% x' = Ax + Bu + Bp*d
% y  = Cx

% x = [id; iq; vd; vq]
% d = [iod; ioq]
% u = -k*x_nau + u_s (Actuación MPC)

A   = [ -Rf/Lf      Wb  -1/Lf       0;
           -Wb  -Rf/Lf      0   -1/Lf;
          1/Cf       0      0      Wb;
             0     1/Cf   -Wb       0];

B   = [   1/Lf      0;      
             0   1/Lf;
             0      0;
             0      0];

Bp  = [      0      0;      
             0      0;
         -1/Cf      0;
             0  -1/Cf];

C   = eye(4);
Cd = C;


% Modelo espacio estado filtro LC (Discreto)
% x(k+1) = Ad*x(k) + Bd*u(k) + Bpd*d(k)
% y(k)   = Cx(k)
[~, Bpd] = c2d(A,Bp,T);
[Ad, Bd] = c2d(A,B,T);

% Matrices de Peso 
%Gamma      = 20*eye(2); % 20 es por tuning
Gamma      = 100*eye(2); % 100 es por tuning
Omega      = eye(4);
% Se le da mayor peso a los estados id iq
Omega(1,1) = 1e2;
Omega(2,2) = 1e2;

% Solución a LQR discreto
[K_inf,Omega_N,~] = dlqr(Ad,Bd,Omega,Gamma);

%% Solver quadprog
solver = "quadprog";
counter = 1;
sim_der = sim("CCS_MPC_ADMM_simscape.slx");
time = sim_der.control_signals.time;
% Control signals
vsdref   = sim_der.control_signals.data(1,1);
vsqref   = sim_der.control_signals.data(1,3);
quadprog_vsd = sim_der.control_signals.data(:,2);
quadprog_vsq = sim_der.control_signals.data(:,4);
% Current boundaries
ismax   = 8;
quadprog_is   = abs(sim_der.current.data(:,1) + 1i*sim_der.current.data(:,2));

%% Solver ADMM
ITERS_ADMM = [1, 10:10:50];
ADMM_vsd = zeros(length(quadprog_vsd),length(ITERS_ADMM));
ADMM_vsq = zeros(length(quadprog_vsq),length(ITERS_ADMM));
ADMM_is = zeros(length(quadprog_vsd),length(ITERS_ADMM));
solver = "ADMM";
for i = 1:length(ITERS_ADMM)
    ADMM_iters = ITERS_ADMM(i);
    x_ADMM = zeros(16,1);
    z_ADMM = zeros(74,1);
    u_ADMM = zeros(74,1);
    counter = 1;
    sim_der = sim("CCS_MPC_ADMM_simscape.slx");
    % Control signals
    ADMM_vsd(:,i) = sim_der.control_signals.data(:,2);
    ADMM_vsq(:,i) = sim_der.control_signals.data(:,4);
    % Current boundaries
    ADMM_is(:,i) = abs(sim_der.current.data(:,1) + 1i*sim_der.current.data(:,2));
    fprintf("ADMM con %d iteraciones\n", ADMM_iters)
end

save('../matfiles/ADMM_iters.mat','vsdref','vsqref','ismax','time', ...
    'quadprog_vsd','quadprog_vsq','quadprog_is', ...
    'ADMM_vsd','ADMM_vsq','ADMM_is');
