# Archivos de simulación en MATLAB/Simulink

:warning: **La versión de MATLAB utilizada es 9.12.0.1956245 (R2022a) Update 2**

<div align="justify">
En esta carpeta se incluyen los archivos requeridos para simular el sistema en MATLAB/Simulink utilizando los addons Simscape Electrical y Optimization Toolbox.
</div>

- ADMM_QP.m: Representación en MATLAB del funcionamiento del solver ADMM.

- CCS_MPC_ADMM_iters.m: Código que simula utilizando tanto quadprog como los seis casos de ADMM para almacenar las señales para cada caso en un matfile.

- CCS_MPC_ADMM_simscape.slx: Esquema de simulación en Simulink.

- CCS_MPC.m: Función que es llamada dentro de la simulación en Simulink llamada CCS_MPC_ADMM_simscape.slx.

- Signal_sim.m: Código que simula utilizando tanto quadprog como los seis casos de ADMM para almacenar las señales de control para graficar.

- Simulink_CCS_MPC_Diagram.pdf: Diagrama del esquema de simulación en Simulink.
