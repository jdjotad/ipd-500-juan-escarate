clear all
close all
clc

global x_ADMM z_ADMM u_ADMM QP_vars counter solver ADMM_iters curr_rest
max_steps = 2501;
QP_vars = {};
QP_vars.io = zeros(max_steps,2,1);
QP_vars.x0 = zeros(max_steps,4,1);
QP_vars.Q = zeros(16,16);
QP_vars.q = zeros(16,1);
QP_vars.A = zeros(74,16);
QP_vars.c = zeros(max_steps,74,1);
QP_vars.x_QP  = zeros(max_steps,16,1);
QP_vars.rho   = zeros(max_steps,1);

%% VALORES BASE:
% Sb=5000 %VA
% Vb=260 %base pico por hp
% Ib=12.8 %A
% Zb=20.3 %ohm  Vb/Ib
% Wb=1 %rad/s  50 Hz nominal
% Vdc=630 %V base dc

%% VALORES NOMINALES (per unit):
% Vs=1 % Vs/Vb
% Rf=0.0032   % Rf/Zb 
% Lf=0.045    % LfWb/Zb
% Cf=0.095    % CfWbZb
% Vdc=1       % Vdc/Vb
% T=200*10^(-6)      % Sampling time

%%
% Valores de Planta laboratorio filtro LC
Rf  = 0.065;    % Resistencia Filtro 
Lf  = 3e-3;     % Inductancia Filtro 
Cf  = 15e-6;    % Capacitancia Filtro 
Vdc = 100;      % Voltaje DC de entrada
T   = 200e-6;   % Periodo de muestreo
f   = 50;       % Frecuencia Hz
Wb  = 2*pi*f;   % Frecuencia rad/s   
RL  = 23.6;     % Resistencia de carga


% Modelo espacio estado filtro LC (Continuo)
% x' = Ax + Bu + Bp*d
% y  = Cx

% x = [id; iq; vd; vq]
% d = [iod; ioq]
% u = -k*x_nau + u_s (Actuación MPC)

A   = [ -Rf/Lf      Wb  -1/Lf       0;
           -Wb  -Rf/Lf      0   -1/Lf;
          1/Cf       0      0      Wb;
             0     1/Cf   -Wb       0];

B   = [   1/Lf      0;      
             0   1/Lf;
             0      0;
             0      0];

Bp  = [      0      0;      
             0      0;
         -1/Cf      0;
             0  -1/Cf];

C   = eye(4);
Cd = C;


% Modelo espacio estado filtro LC (Discreto)
% x(k+1) = Ad*x(k) + Bd*u(k) + Bpd*d(k)
% y(k)   = Cx(k)
[~, Bpd] = c2d(A,Bp,T);
[Ad, Bd] = c2d(A,B,T);

% Matrices de Peso 
%Gamma      = 20*eye(2); % 20 es por tuning
Gamma      = 100*eye(2); % 100 es por tuning
Omega      = eye(4);
% Se le da mayor peso a los estados id iq
Omega(1,1) = 1e2;
Omega(2,2) = 1e2;

% Solución a LQR discreto
[K_inf,Omega_N,~] = dlqr(Ad,Bd,Omega,Gamma);

%% Solver quadprog
solver = "quadprog";
counter = 1;
sim_der = sim("CCS_MPC_ADMM_simscape.slx");
time = sim_der.control_signals.time;
x_QP = QP_vars.x_QP;
% Control signals
vsdref   = sim_der.control_signals.data(1,1);
vsqref   = sim_der.control_signals.data(1,3);
quadprog_vsd = sim_der.control_signals.data(:,2);
quadprog_vsq = sim_der.control_signals.data(:,4);
quadprog_vsa = sim_der.control_signals.data(:,5);
quadprog_vsb = sim_der.control_signals.data(:,6);
quadprog_vsc = sim_der.control_signals.data(:,7);
% Current boundaries
ismax   = 8;
quadprog_is   = abs(sim_der.current.data(:,1) + 1i*sim_der.current.data(:,2));

%% Solver ADMM
ITERS_ADMM = [1, 10:10:50];
solver = "ADMM";
ADMM_vsd = zeros(length(quadprog_vsd),length(ITERS_ADMM));
ADMM_vsq = zeros(length(quadprog_vsq),length(ITERS_ADMM));
ADMM_vsa = zeros(length(quadprog_vsa),length(ITERS_ADMM));
ADMM_vsb = zeros(length(quadprog_vsb),length(ITERS_ADMM));
ADMM_vsc = zeros(length(quadprog_vsc),length(ITERS_ADMM));
ADMM_is = zeros(length(quadprog_vsd),length(ITERS_ADMM));
for i = 1:length(ITERS_ADMM)
    ADMM_iters = ITERS_ADMM(i);
    x_ADMM = zeros(16,1);
    z_ADMM = zeros(74,1);
    u_ADMM = zeros(74,1);
    counter = 1;
    sim_der = sim("CCS_MPC_ADMM_simscape.slx");
    x0 = QP_vars.x0;
    io = QP_vars.io;
    A = QP_vars.A;
    Q = QP_vars.Q;
    q = QP_vars.q;
    c = QP_vars.c;
    x_QP_ADMM = QP_vars.x_QP;
    R_inv = QP_vars.R_inv;
    rho = mean(QP_vars.rho);
    fprintf("ADMM con %d iteraciones\n", ADMM_iters)
    fprintf("rho promedio = %f\n", mean(QP_vars.rho))
    
    % Control signals
    ADMM_vsd(:,i) = sim_der.control_signals.data(:,2);
    ADMM_vsq(:,i) = sim_der.control_signals.data(:,4);
    ADMM_vsa(:,i) = sim_der.control_signals.data(:,5);
    ADMM_vsb(:,i) = sim_der.control_signals.data(:,6);
    ADMM_vsc(:,i) = sim_der.control_signals.data(:,7);
    % Current boundaries
    ADMM_is(:,i) = abs(sim_der.current.data(:,1) + 1i*sim_der.current.data(:,2));

    filename = sprintf("../matfiles/ADMM_model_%d_iters.mat", ADMM_iters);
    save(filename,'x0','io','Q','q','A','c','R_inv','x_QP','x_QP_ADMM','rho');
end

%% THD
THD_qp      = [thd(quadprog_vsa(1001-5*100+1:1001),50,50), ... 
               thd(quadprog_vsb(1001-5*100+1:1001),50,50), ...
               thd(quadprog_vsc(1001-5*100+1:1001),50,50)];
THD_qp_sat  = [thd(quadprog_vsa(end-5*100+1:end),50,50), ... 
               thd(quadprog_vsb(end-5*100+1:end),50,50), ...
               thd(quadprog_vsc(end-5*100+1:end),50,50)];
THD_ADMM     = zeros(3,length(ITERS_ADMM));
THD_ADMM_sat = zeros(3,length(ITERS_ADMM));
for i = 1:length(ITERS_ADMM)
    THD_ADMM(:,i)     = [thd(ADMM_vsa(1001-5*100+1:1001,i),50,50), ... 
                         thd(ADMM_vsb(1001-5*100+1:1001,i),50,50), ...
                         thd(ADMM_vsc(1001-5*100+1:1001,i),50,50)];
    THD_ADMM_sat(:,i) = [thd(ADMM_vsa(end-5*100+1:end,i),50,50), ... 
                         thd(ADMM_vsb(end-5*100+1:end,i),50,50), ...
                         thd(ADMM_vsc(end-5*100+1:end,i),50,50)];
end
save('../matfiles/ADMM_Control_Signals.mat', ...
    'vsdref','vsqref','ismax','time', ...
    'quadprog_vsd','quadprog_vsq', ...
    'quadprog_vsa','quadprog_vsb','quadprog_vsc', ...
    'quadprog_is', ...
    'ADMM_vsd','ADMM_vsq', ...
    'ADMM_vsa','ADMM_vsb','ADMM_vsc', ...
    'ADMM_is', 'THD_qp', 'THD_qp_sat','THD_ADMM', 'THD_ADMM_sat');
    


