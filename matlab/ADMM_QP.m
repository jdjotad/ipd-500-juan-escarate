function [x, z, u] = ADMM_QP(Q, q, A, c, init_x, init_z, init_u, opt_rho, opt_iters)
    global QP_vars counter
    % ------ QP PROBLEM ------ %
    % Min: 1/2*x^T*Q*x + q^T*x
    % ST:  Ax <= c
    % 
    % ADMM Reformulation
    % Min: 1/2*x^T*Q*x + q^T*x + g(z)
    % ST:  Ax + z = c
    % g(z) is the indicator function of Z:
        % g(z) = 0 if z in Z,
        % g(z) = ∞ if any component of z not in Z
    %
    % Q -> R^(n x n)
    % A -> R^(n x n)
    % q -> R^(n)
    % c -> R^(n)
    % x -> R^(n)
    % z -> R^(n) (slack variable)
    
    % init_x -> Initial x point
    % init_z -> Initial z point
    % init_u -> Initial u point
    % rho -> Rho value for Augmented Lagrangian
    % iters -> Maximum iterations
    
    % ------ CHECK NUMBER OF INPUT ARGUMENTS ------ %
    if nargin < 4 || nargin > 9
        errorStruct.message = 'Incorrect number of inputs';
        errorStruct.identifier = 'ADMM_QP:incorrectInputs';
        error(errorStruct);
    end
    
    % Fill in unset optional values.
    %test_rho(A,Q);
    QP_vars.rho(counter) = dhang_rho(A,Q);
    rho = QP_vars.rho(counter);
    iters = 50;
    switch nargin
        case 8
            rho = opt_rho;
        case 9
            rho = opt_rho;
            iters = opt_iters;
    end
    
    % ------ DATA INITIALIZATION ------ %
    % Length of variables
    n = size(q,1);
    % Quantity of restrictions
    n_c = size(c,1);
    % Data initialization
    if nargin > 4
        x = init_x;
        z = init_z;
        u = init_u;
    else
        x = zeros(n,1);
        z = zeros(n_c,1);
        u = zeros(n_c,1);
    end
    % ------ ADMM FORMULATION ------ %

    % Augmented Lagrangian scaled form
    % L_rho(x, z, u) = f(x) + g(z) + (rho/2)*||Ax + z - c + u||_2^2
    % where u -> y/rho (scaled dual variable)

    % X minimization
        % To minimize we should calculate d(L_rho)/dx derivative
        % d(L_rho)/dx = f'(x) + rho*A^T(Ax + v) = 0
        % where v -> z - c + u
        % d(L_rho)/dx = Qx + q^T + rho*A^T(Ax + v) = 0
        % x = (Q + rho*A^T*A)^-1 * (-rho*A^T*v - q^T)

    % Z minimization
        %Usually evaluating Projection_Z(z) is inexpensive; 
        % for example, if Z = [α, β] is an interval,
        % Projection_Z(z) = min{max{z,α},β}
        % In this case Z = [0, inf) so
        % Projection_Z(z) = max{z,0}
        
    R = Q + rho*(A'*A);
    R_inv = R \ eye(size(R,1));
    if(counter == 1)
        QP_vars.R_inv = R_inv;
    end
    % Iterations
    for k = 1:iters
        v_x = z - c + u;
        x = R_inv * (-rho*A'*v_x - q);    % update x
           
        z = max(0, -A*x - u + c);             % update z

        % Then we update the scaled dual variable
        u = u + (A*x + z - c);                % update u
    end

end

function rho = dhang_rho(A,Q)
    % ------ QP PROBLEM ------ %
    % Min: 1/2*x^T*Q*x + q^T*x + g(z)
    % ST:  Ax + z = c
    %      z >= 0
    % g(z) is the indicator function of Z:
        % g(z) = 0 if z in Z,
        % g(z) = ∞ if any component of z not in Z

    % Define the singular decomposition of A as
    % A = U*S*V'
    % where U and V are orthonormal and S is diagonal with positive
    % real matrix entries
    [~,S,V] = svds(A);
    % Set Sd = (S'*S)^(-1/2)
    Sd = (S'*S)^(-1/2);
    % Calculate Pd as 
    % Pd = Sd'*V'*Q*V*Sd
    Pd = Sd'*V'*Q*V*Sd;
    % Calculate non-zero eigenvalues
    lambda_Pd  = eig(Pd);
    lambda_max = max(lambda_Pd); 
    beta_1     = 1/lambda_Pd(1);
    beta_2     = 1/lambda_max;
    % Solve eq 36 from Dang paper (Embedded ADMM-based 
    % QP Solver for MPC with polytopic constraints)
    % a4*rho^4 + a3*rho^3 + a2*rho^2 + a1*rho + a0
    a4 =  beta_1*beta_2*(beta_1^2 + beta_2^2);
    a3 = -(beta_1^3 + beta_2^3 - 3*beta_1*beta_2*(beta_1 + beta_2));
    a2 = -(beta_1 - beta_2)^2;
    a1 = -2*(beta_1 + beta_2);
    a0 = -2;
    eq_roots = roots([a4, a3, a2, a1, a0]);
    rho = abs(eq_roots(1));
end